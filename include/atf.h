/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * <thanhpv2@house3d.org> wrote this code. As long as you retain this
 * notice, you can do whatever you want with this stuff. If we
 * meet someday, and you think this stuff is worth it, you can
 * buy me a beer in return.		Thanh Pham Van
 * ------------------------------------------------------------
 */

/*
 * atf.h
 *
 *  Created on: Aug 30, 2018
 *      Author: thanhpv
 */

#ifndef ATF_H_
#define ATF_H_

#include <stdio.h>
#include <cmath>

#include <memory>

#define ZAT_RGB888 0
#define ZAT_RGBA8888 0
#define ZAT_COMPRESSED 2
#define ZAT_RAW_COMPRESSED 3
#define ZAT_COMPRESSED_WITH_ALPHA 4
#define ZAT_COMPRESSED_LOSSY 0x0c
#define ZAT_COMPRESSED_WITH_ALPHA 0x0d

#define ZAT_DATA_MAX_SIZE 16777215
#define ZAT_DATA_MIN_SIZE 0

typedef unsigned short ushort;
typedef unsigned char byte; // using uchar to represent a single byte

struct ZATLength {
	byte first;
	byte second;
	byte third;
};

struct ZATHeader {
	const char * name = "ATF"; // ZAT la dang file duoc sua lai tu ATF - adobe texture format
	ZATLength dataLength;
};

struct ZATData {
	ushort format = ZAT_RAW_COMPRESSED; // file ZAT luon duoc nen duoi dang 3 - RAW Compressed
	ushort log2Width; // width = 2 ^ binLogWidth
	ushort log2Height; // height = 2 ^ binLogHeight
	ZATLength compressedDataLength;
};

inline ZATLength cvtZatLength(uint size) {
	assert(size > ZAT_DATA_MIN_SIZE && size <= ZAT_DATA_MAX_SIZE);
	uint first = size / 65536;
	uint second = (size & 65536) / 255;
	uint third = size - first * 65536 - second * 255;

	ZATLength z;
	z.first = first;
	z.second = second;
	z.third = third;

	return z;
};


bool writeZat(const char * filename, const void * result, const uint width,
		const uint height) {
	std::shared_ptr<FILE> fp (fopen(filename, "wb"));

	if (fp == 0) {
		printf("Error, unable to open output image <%s>\n", filename);
		return false; // or throw an exception
	}

	uint compressedSize = (width / 4) * (height / 4) * 8;
	uint zatDataSize = sizeof(ZATData) + compressedSize;

	ZATHeader header;
	header.dataLength = cvtZatLength(zatDataSize);

	if (compressedSize < ZAT_DATA_MIN_SIZE || compressedSize > ZAT_DATA_MAX_SIZE) {
		printf("Invalid image (width or height <= 0) or image size too large");
		return false; // or... throw an exception
	}

	ushort binLogW = (ushort) log(width);
	ushort binLogH = (ushort) log(height);

	ZATData zatData;
	zatData.log2Height = binLogH;
	zatData.log2Width = binLogW;
	zatData.compressedDataLength = cvtZatLength(compressedSize);

	fwrite(&header, sizeof(ZATHeader), 1, fp.get());
	fwrite(&zatData, sizeof(ZATData), 1, fp.get());
	fwrite(result, compressedSize, 1, fp.get());

	// completed!
	fclose(fp.get());
	return true;
};

#endif /* ATF_H_ */
