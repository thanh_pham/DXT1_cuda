/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * <thanhpv2@house3d.org> wrote this code. As long as you retain this
 * notice, you can do whatever you want with this stuff. If we
 * meet someday, and you think this stuff is worth it, you can
 * buy me a beer in return.		Thanh Pham Van
 * ------------------------------------------------------------
*/

/*
 * cv2_decoder.h
 *
 *  Created on: Aug 29, 2018
 *      Author: thanhpv
 */

#ifndef CV2_DECODER_H_
#define CV2_DECODER_H_

#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/types.hpp>

#define PRINT_DEBUG 1
#define FORCE_RESIZE 0

#if PRINT_DEBUG
#include <iostream>
#endif

#define ALPHA_CHANNEL_DEFAULT 0xFF

bool cv2LoadImage(const char* filepath,
		unsigned char **data,
		unsigned int *W,
		unsigned int *H);

#endif /* CV2_DECODER_H_ */
