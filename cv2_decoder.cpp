/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * <thanhpv2@house3d.org> wrote this code. As long as you retain this
 * notice, you can do whatever you want with this stuff. If we
 * meet someday, and you think this stuff is worth it, you can
 * buy me a beer in return.		Thanh Pham Van
 * ------------------------------------------------------------
*/

#include "cv2_decoder.h"

bool cv2LoadImage(const char* filepath,
		unsigned char **data,
		unsigned int *W,
		unsigned int *H)
{
	cv::Mat image;

	cv::Mat img =  cv::imread(filepath, cv::IMREAD_COLOR);

	img.convertTo(image, CV_8U);
	if (image.type() == CV_8UC4) {
		cv::cvtColor(image, image, cv::COLOR_RGBA2RGB);
	}


	if (image.cols % 4 != 0 || image.rows % 4 != 0) {
#if FORCE_RESIZE
		// Harmful code
		int x = ((uint) ((image.cols / 4) + 0.5)) * 4;
		int y = ((uint) ((image.rows / 4) + 0.5)) * 4;

		cv::resize(image, image, cv::Size(x, y));
#if PRINT_DEBUG
		std::cout << "Resize image to <w=" << x <<", h=" << y << ">" << std::endl;
#endif
		*W = x;
		*H = y;
#else
		return false;
#endif
	} else {
		*W = image.cols;
		*H = image.rows;
	}

	size_t imgSize = *W * *H;
	*data = (unsigned char *) malloc(sizeof(unsigned char) * imgSize * 4);

	unsigned char *ptr = *data;

	for (int i = 0; i < *W; ++i) {
		for (int j = 0; j < *H; ++j) {
			cv::Vec3b color = image.at<cv::Vec3b>(i, j);
			*ptr = color[2]; ++ptr;
			*ptr = color[1]; ++ptr;
			*ptr = color[0]; ++ptr;
			*ptr = 0; ++ptr;
		}
	}

	return true;
}
